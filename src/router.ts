import { Application } from "express";
import HomeController from "./controllers/HomeController";
import CitationController from "./controllers/CitationController";
export default function route(app: Application)
{
    /** Static pages **/
    app.get('/', (req, res) =>
    {
        HomeController.index(req, res);
    });

    /**
     * Affiche la liste des citations
     * @param req 
     * @param res 
     */

     app.get('/citation-all', (req, res) =>
     {
         CitationController.indexDeux(req, res)
     });
 
     app.get('/citation-create', (req, res) =>
     {
         CitationController.showForm(req, res)
     });

     app.post('/citation-create', (req, res) =>
     {
        CitationController.create(req, res)
     });

     app.get('/citation-read/:id', (req, res) =>
    {
        CitationController.read(req, res)
    });

    app.get('/citation-update/:id', (req, res) =>
    {
        CitationController.showFormUpdate(req, res)
    });

    app.post('/citation-update/:id', (req, res) =>
    {
        CitationController.update(req, res)
    });

    app.get('/citation-delete/:id', (req, res) =>
    {
        CitationController.delete(req, res)
    });

    





    
}
