import { Request, Response } from "express-serve-static-core";
import { db } from "../server";

export default class HomeController
{

    /**
     * Affiche la liste des citations
     * @param req 
     * @param res 
     */
    static index(req: Request, res: Response): void
    {
console.log(req.params);

        const citationEcrivain = db.prepare('Select * FROM Auteurs WHERE id = ? ').get(req.params.Auteurs_id)

        console.log(citationEcrivain)

        const ecrivCitation = db.prepare('Select * FROM Citations WHERE id = ?').all(citationEcrivain.Auteurs_id)

        console.log(ecrivCitation);
        

        res.render('pages/index', {
            title: "Citations de l'auteur",
            
        });
    }

}