import { Request, Response } from "express-serve-static-core";
import { request } from "http";
import { db } from "../server";

export default class CitationController 
{
    /**
     * Affiche la liste des citations
     * @param req 
     * @param res 
     */
    static indexDeux(req : Request, res : Response)
    {
        
        const citations = db.prepare('SELECT * FROM Citations ORDER BY id DESC limit 1').all()
       console.log(citations)

       const exemple = db.prepare('SELECT * FROM Auteurs WHERE id = ?').get(citations[0].Auteurs_id);
       console.log(exemple);
        
       
        
       
    

        res.render('pages/IndexDeux', {
            title : 'Voir les citattion',
            citation : citations,
            auteur : exemple
            
        })

    
    }

    /**
     * Affiche le formulaire de creation d'article
     * @param req 
     * @param res 
     */

    static showForm(req : Request, res : Response): void 
    {
        res.render('pages/citation-create');
    }

    /**
     * Recupere le formulaire et insere l'article en db
     * @param req 
     * @param res 
     */

    static create(req : Request, res : Response): void
    {

       
        db.prepare('INSERT INTO Citations ("Auteurs_id", "content") VALUES (?,?)').run(1, req.body.content);

        CitationController.indexDeux(req, res);
    }

    /**
     * Affiche 1 article
     * @param req 
     * @param res 
     */

    static read(req : Request, res : Response)
    {

        
        const citation = db.prepare('SELECT * FROM Citations').all();

        res.render('pages/citation', {
            liste : citation
        })

    }

    /**
     * Affiche le formulaire pour modifier un article
     * @param req 
     * @param res 
     */

    static showFormUpdate(req : Request, res : Response)
    {

       
        const citation = db.prepare('SELECT * FROM Citations WHERE id = ?').get(req.params.id);

        res.render('pages/citation-update', {
            liste : citation
        })

    }

    /**
     * Recupere le formulaire de l'article modifié et l'ajoute a la database
     * @param req 
     * @param res 
     */

    static update(req :Request, res: Response)
    {
        

        db.prepare('UPDATE Citations SET Auteurs_id = ?, content = ? WHERE id = ?').run(req.body.title, req.body.content, req.params.id);
    }

    /**
     * Supprime un article
     * @param req 
     * @param res 
     */

    static delete(req : Request, res: Response)
    {
       

        db.prepare('DELETE FROM Citations WHERE id = ?').run(req.params.id);

        CitationController.indexDeux(req, res);
    }
}

function prepare(arg0: string) {
    throw new Error("Function not implemented.");
}
