-- Creator:       MySQL Workbench 6.3.8/ExportSQLite Plugin 0.1.0
-- Author:        Bensaci
-- Caption:       New Model
-- Project:       Name of the project
-- Changed:       2022-02-21 15:55
-- Created:       2022-02-21 15:55
PRAGMA foreign_keys = OFF;

-- Schema: mydb

BEGIN;
CREATE TABLE "Auteurs"(
  "id" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
  "nom" VARCHAR(45) NOT NULL
);
CREATE TABLE "Citations"(
  "id" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
  "content" VARCHAR(45) NOT NULL,
  "Auteurs_id" INTEGER NOT NULL,
  CONSTRAINT "fk_Citations_Auteurs"
    FOREIGN KEY("Auteurs_id")
    REFERENCES "Auteurs"("id")
);
CREATE INDEX "Citations.fk_Citations_Auteurs_idx" ON "Citations" ("Auteurs_id");
COMMIT;
